# tigrock

Resolving remote machine's IP addresses trough Telegram.

A (really) poor man's alternative to DNS.

## Usage

1. create a Telegram channel

1. register a Telegram bot through @BotFather

1. add the bot to your channel

1. fire up the bot with

    `$ tigrok  machine_name  bot_token  channel_id`

    (or by using environment variables)

You can use the same bot on several machines* because it only ever publish and never reads

*: depending on Telegram bot API limits

## Options

Options are set trough either a __secrets.py file, environment variables or positional arguments


environment variables: 

- `TIGROK_NAME`  

- `TIGROK_CHANNEL`  

- `TIGROK_TOKEN`  

- `TIGROK_INTERVAL`


## Results

the bot will periodically publish on the Telegram channel something like this:

    `{  "ip": (ipv4 or ipv6), 
        "bot": 1234567899, 
        "machine": (uuid), 
        "errors": (optional array of {
            "timestamp": (epoch), 
            "message": (error text)})
    }`

## TODO

- [ ] better CLI with flags  

- [ ] accept other exernal IP APIs  
