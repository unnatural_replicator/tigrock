#!/usr/bin/env python3

'''tigrok

resolving remote machine's IP addresses trough Telegram

sends the machine external IP periodically to the dedicated Telegram channel
the machine id can be later searched through queries in the messages

the messages have this structure:
{"ip": (ipv4 or ipv6), "bot": 1234567899, "machine": (uuid), "errors": (optional array of {"timestamp": (epoch), "message": (error text)})}

usage:
args: [name, [bot_token, [channel_id]]]
environment variables: TIGROK_NAME, TIGROK_CHANNEL, TIGROK_TOKEN, TIGROK_INTERVAL'''


from urllib.request import urlopen, Request
from urllib.error import URLError
from urllib.parse import urlencode
from json import loads, dumps
from asyncio import sleep, get_event_loop
from os import environ
from sys import argv, exit, stderr
from time import time
from uuid import UUID, getnode
try:
    from .secrets import bot_token, channel_id
except ImportError as e:
    print(e.msg)
    bot_token:str = ''
    channel_id:str = ''


try:
    sleep_time_s = environ['TIGROK_INTERVAL']
except KeyError:
    sleep_time_s = 900


install_name:str = str(UUID(int=getnode()))
try:
    install_name = argv[1] 
except IndexError:
    try :
        install_name = environ['TIGROK_NAME'] 
    except KeyError:
        try:
            install_name = environ['HOSTNAME'] 
        except KeyError:
            pass

try:
    bot_token = argv[2]
except IndexError:
    try: 
        bot_token = environ['TIGROK_TOKEN']
    except KeyError:
        pass
if not bot_token:
    exit(-1)


try:
    channel_id = str(int(argv[3]))
except:
    try: 
        channel_id = environ['TIGROK_CHANNEL']
    except KeyError:
        pass
if not channel_id:
    exit(-1)


async def send_ip()->None:
    errors = []
    api_url = 'https://api64.ipify.org?format=json'
    bot_url = f'https://api.telegram.org/bot{bot_token}/sendMessage'
    bot_info_url = f'https://api.telegram.org/bot{bot_token}/getMe'
    req = Request(bot_info_url)    
    resp = urlopen(req).read()
    bot_info_dict = loads(resp)['result']
    bot_id = bot_info_dict['id'] 

    while True:
        message = {}
        try:
            message = loads(urlopen(api_url).read().decode('utf8'))
        except URLError as e:
            errors.append({'timestamp': time(), 'message': 'URLError '+ api_url})
        except Exception as e:
            errors.append({'timestamp': time(), 'message': 'Error fetching ' + api_url + " : " + e.args[0]})
        message['bot'] = bot_id
        message['machine'] = install_name
        if errors:
            message['errors'] = errors

        post_data=urlencode({'chat_id': channel_id, 'text': dumps(message)}).encode()
        try:
            req = Request(bot_url, data=post_data, method='POST')            
            resp = urlopen(req)
            if not resp.status == 200:
                raise RuntimeError
        except Exception as e:
            print(e.args[0], file=stderr)
            
        else:
            errors.clear()

        await sleep(sleep_time_s)


loop = get_event_loop()
task = loop.create_task(send_ip())

try:
    loop.run_until_complete(task)
except KeyboardInterrupt:
    exit(0)
